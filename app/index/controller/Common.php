<?php

namespace app\index\controller;

use app\model\DomainInfo;
use think\exception\HttpResponseException;
use think\facade\View as FacadeView;
use think\response\View;

class Common extends BaseController
{
    public function initialize()
    {
        parent::initialize();

        $current_host = $this->request->host();

        $model_domain_info = DomainInfo::where('domain', $current_host)->cache(60)->find();

        if (!empty($model_domain_info)) {
            $this->throwDomainInfoContent($model_domain_info);
        }

    }

    public function throwDomainInfoContent($model_domain_info)
    {
        FacadeView::assign('model_domain_info', $model_domain_info);
        $content = FacadeView::fetch('index/domain_info');
        $response = View::create($content);

        throw new HttpResponseException($response);
    }
}
